import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*
Написать функцию, которая перебирает целые числа от 0 до бесконечности и выводит на экран, через запятую,
те, которые делятся без остатка на количество разрядов (знаков) этого числа (т.е. те двухзначные,
которые делятся на 2, трехзначные на три и т.д.). Выполнение обработки должно остановиться в тот момент,
когда всего будет выведено N (задается пользователем) таких чисел.
*/
public class Unit {
    public static void main(String... args) {

        int userInput;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите целое число");
        userInput = scan.nextInt();

        func(userInput);
    }
    private static void func(int userInput) {
        int i = 0;
        ArrayList<Integer> list = new ArrayList<>(); //список чисел, делящихся на свой разряд
        while (list.size() < userInput) {
            if (i % Integer.toString(i).length() == 0) {
                list.add(i);
            }
            i++;
        }
        System.out.println(list);
        System.out.println("количество элементов в списке " + list.size());
    }
}


